Hi all,


As you may know I received an email from Michael Ryan on Friday requesting some action in relation to the assignments. I am going to propose the following adjustments to the project requirements:

1) There is no need to present your work in the form of a ppt presentation on a video. This is now dropped.

2) Instead of producing a 10 page report you can submit a google colab or jupyter  notebook. Make sure I have access to the data.

3) The dataset does not have to be novel.  Pick one from Kaggle for example. The expectation is for you to implement some of the methods you have learnt or use something you have read about. Don't worry about the 100,000 record stipulation (this is now dropped).  

4) If you have already got your project completed or near completion based on the original spec then I will take this into consideration on your marks.

5) I have set up a zoom class that you can all attend at 4pm on Wednesday. I would ask you to put your questions in the following google doc before the class so we can structure it. If you can't make the class I will record the session for you.

6) Finally don't copy other peoples work as your content will be checked for plagiarism. 
 
I hope this helps and keep safe,
